﻿using FirstMvc5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMvc5.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie
        public ActionResult Random()
        {
            var movies = new Movie()
            {
                Id=123,
                Name = "Shrek"
            };

            return View();
        }


        public ActionResult AddMovie(int id) {
            var model = new Movie();
            if (id == 123)
            {
                model.Id = 123;
                model.Name = "Shrek"; 
            }
            return View(model);
        }
        public ActionResult AddMovie(Movie model) {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose();
        }
    }
}